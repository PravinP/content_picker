class CreateContents < ActiveRecord::Migration[5.0]
  def change
    create_table :contents do |t|
      t.string :tag_name, limit: 2 # limit 2 for header h1/h2/h3/a
      t.text :content # will store the url of link tag also in content.
      t.references :page, index: true
      t.timestamp
    end
  end
end
