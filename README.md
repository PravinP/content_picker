# README

This application is useful to captures content for h1, h2, h3 and a (link/anchor) tags from an given URL. It is based on JSON apis.

Things you may want to cover:

* Ruby version
  2.4.0

* How to run the test suite
  * Move into application root
  cd content_picker
  bin/rails test test/

* Services (job queues, cache servers, search engines, etc.)
  N/A

* Deployment instructions
  get clone of application
  git clone URL
  
  change directory to application root
  cd content_picker
  
  Install required gems(libraries)
  bundle install
  
  To create database
  rake db:create

  To create schema
  rake db:migrate

* Its easy to extend the usecases for capturing various HTML contents in this application by slight modfications.

* Instructions to use this application

Followig are the end points to use this:

1. To List all URL and captured content

REQUEST:
method: get
url: http://[HOST_NAME]:[PORT]/pages

PARAMS: N/A

SAMPLE OUTPUT:
{
  "success": true,
  "pages": [
    {
      "id" : 1,
      "url": "http://myexample1hostname.com",
      "contents": [
        {
          "tag_name": "a",
          "content": "http://myexample1hostname.com/advanced_search?hl=en-IN&authuser=0"
        }
      ],
      "created_at": "2017-01-21T12:02:50.875Z"
    }
  ]
}


2. To add new URL to capture the content

REQUEST:
method: post
url: http://[HOST_NAME]:[PORT]/pages

PARAMS:
{page: {url: "SOME URL"}}

SAMPLE OUTPUT:
{
  "success": true,
  "page": {
    "id": 3,
    "url": "http://example2hostname.com",
    "contents": [
      {
        "tag_name": "a",
        "content": "http://example2hostname.com/advanced_search?hl=en-IN&authuser=0"
      }
    ],
    "created_at": "2017-01-21T14:04:17.082Z"
  }
}