require 'test_helper'

class PagesControllerTest < ActionDispatch::IntegrationTest
  
  test "should create page" do
    post pages_path, params: {page: {url: "http://google.com"}}
    assert_response :success
    res_body = JSON.parse @response.body
    assert_equal true, res_body["success"]
  end

  test "should not create page" do
    post pages_path, params: {page: {url: ""}}
    assert_response :success
    res_body = JSON.parse @response.body
    assert_equal false, res_body["success"]
  end  

  test "should list pages" do
    get pages_url
    assert_response :success
    res_body = JSON.parse @response.body
    assert_equal 2, res_body.length
  end  
end
