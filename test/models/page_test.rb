require 'test_helper'

class PageTest < ActiveSupport::TestCase
  
  test "should not save page without url" do
    page =Page.new
    assert_not page.save
  end

  test "should not save page with invalid url" do
    page =Page.new(url: "invalid")
    assert_not page.save
  end

  test "should not save page if url not accessible" do
    page =Page.new(url: "http://test.abcde")
    assert page.save
  end

  test "should not save page with valid url" do
    page =Page.new(url: "http://google.com")
    assert page.save
  end

end
