class Content < ApplicationRecord
  # ASSOCIATIONS
  belongs_to :page

  # VALIDATIONS
  validates :tag_name, presence: true

  def as_json options={}
    {tag_name: tag_name, content: content}
  end

end
