class Page < ApplicationRecord
  require 'open-uri'
  # ASSOCIATIONS
  has_many :contents, -> {order :tag_name}

  # VALIDATIONS
  validates :url, presence: true, format: URI::regexp(%w(http https))

  # CALLBACKS
  after_create :get_and_store_content

  # CONSTANTS
  TAGS = ["h1", "h2", "h3"]

  def get_and_store_content
    begin
      page = Nokogiri::HTML(open(url))
      tag_content = []
      page.css(TAGS.join(',')).each do |tag_obj|
        tag_content << Content.new({tag_name: tag_obj.name, content: tag_obj.text})
      end
      page.css('a/@href').each do |link_obj|
        tag_content << Content.new({tag_name: 'a', content: complete_url(link_obj.value)})
      end
      self.contents << tag_content
    rescue SocketError,HttpError => e
      errors.add(:url, "not accessible." )
      return false
    rescue Exception => e
      errors.add(:url, "something went wrong with the request. Please try again after some time" )
      return false
    end
  end

  def complete_url(link)
    link = link.gsub('javascript:void(0);','')
    link = link.gsub('javascript:void(0)','')
    uri = URI.parse(link)
    if uri.host.blank? 
      link.to_s.start_with?("/") ? "#{base_url}#{link}" : "#{base_url}/#{link}"
    else
      link
    end
  end

  def base_url
    uri = URI.parse(url)
    "#{uri.scheme}://#{uri.host}"
  end

  def as_json options={}
    {id: id, url: url, contents: contents, created_at: created_at}
  end

end
