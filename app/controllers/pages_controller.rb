class PagesController < ApplicationController
  def index
    render json: {success: true, pages: Page.all}
  end

  def create
    page = Page.new(page_params)
    if page.save
      render json: {success: true, page: page}
    else
      render json: {success: false, message: page.errors.full_messages}
    end
  end

  private

  def page_params
    params.require(:page).permit([:url])
  end

end
